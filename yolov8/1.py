
import torch
import multiprocessing
import sys
import threading
import time
import cv2
import torch.multiprocessing as mp
import logging
from PyQt5.QtWidgets import QApplication, QMainWindow, QFileDialog, QLabel, QProgressBar
from PyQt5.QtGui import QPixmap, QImage
from PyQt5.QtCore import Qt, QThread, pyqtSignal
from PyQt5 import QtCore
from ultralytics import YOLO

from untitled import Ui_MainWindow


class LogRedirector:
    def __init__(self, signal, batch_size=5, flush_interval=1):
        self.signal = signal
        self.batch_size = batch_size
        self.flush_interval = flush_interval
        self.log_buffer = []
        self.last_flush_time = time.time()

    def write(self, message):
        if message.strip():  # 去除空行
            self.log_buffer.append(message)

        # 每当日志数量达到batch_size或flush_interval超时，就发送日志
        current_time = time.time()
        if len(self.log_buffer) >= self.batch_size or (current_time - self.last_flush_time) >= self.flush_interval:
            self.flush()

    def flush(self):
        if self.log_buffer:
            # 批量发送日志
            self.signal.emit('\n'.join(self.log_buffer))
            self.log_buffer = []  # 清空日志缓冲区
            self.last_flush_time = time.time()

    def close(self):
        self.flush()

class TrainingThread(QThread):
    output_signal = pyqtSignal(str)
    finished_signal = pyqtSignal()

    def __init__(self, model_path, yaml_path, epochs, imgsz, batch_size, learning_rate, device, workers, momentum, weight_decay, export_onnx):
        super().__init__()
        self.model_path = model_path
        self.yaml_path = yaml_path
        self.epochs = epochs
        self.imgsz = imgsz
        self.batch_size = batch_size
        self.learning_rate = learning_rate
        self.device = device if torch.cuda.is_available() else 'cpu'
        self.workers = workers
        self.momentum = momentum
        self.weight_decay = weight_decay
        self.export_onnx = export_onnx

    def run(self):
        try:
            sys.stdout = LogRedirector(self.output_signal, batch_size=5, flush_interval=2)
            sys.stderr = sys.stdout  # 重定向错误输出到相同的日志处理

            model = YOLO(self.model_path)
            print(f"模型 {self.model_path} 已加载。")
            print(f"开始训练，epochs: {self.epochs}, imgsz: {self.imgsz}, batch: {self.batch_size}, lr0: {self.learning_rate}, device: {self.device}")

            model.train(
                data=self.yaml_path,
                epochs=self.epochs,
                imgsz=self.imgsz,
                batch=self.batch_size,
                lr0=self.learning_rate,
                device=self.device,
                workers=self.workers,
                momentum=self.momentum,
                weight_decay=self.weight_decay
            )

            metrics = model.val()
            print(f"训练完成。评估结果: {metrics}")

            if self.export_onnx:
                path = model.export(format="onnx")
                print(f"模型导出至 ONNX 格式，路径为 {path}")

        except Exception as e:
            print(f"训练过程中出现错误: {str(e)}")
        finally:
            sys.stdout.flush()
            sys.stdout = sys.__stdout__
            sys.stderr = sys.__stderr__
            self.finished_signal.emit()

class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.setupUi(self)

        self.model_paths = {
            "yolov8n.pt": "yolov8n.pt",
            "yolov8s.pt": "yolov8s.pt",
            "yolov8m.pt": "yolov8m.pt",
            "yolov8l.pt": "yolov8l.pt"
        }

        self.model_path = ""
        self.yaml_path = ""
        self.trained_model_path = ""
        self.cap = None
        self.timer = None
        self.textEdit_3.setPlainText("50")
        self.textEdit_4.setPlainText("640")
        self.textEdit_5.setPlainText("16")
        self.textEdit_6.setPlainText("0.01")
        self.textEdit_7.setPlainText("0")
        self.textEdit_8.setPlainText("4")
        self.textEdit_9.setPlainText("0.937")
        self.textEdit_10.setPlainText("0.0005")

        self.pushButton.clicked.connect(self.switch_to_train_page)
        self.pushButton_2.clicked.connect(self.switch_to_predict_page)
        self.comboBox.addItems(["yolov8n.pt", "yolov8s.pt", "yolov8m.pt", "yolov8l.pt"])
        self.pushButton_4.clicked.connect(self.select_yaml)
        self.pushButton_5.clicked.connect(self.start_training_process)
        self.checkBox.stateChanged.connect(self.toggle_onnx_export)
        self.pushButton_6.clicked.connect(self.select_trained_model)
        self.pushButton_7.clicked.connect(self.predict_image)
        self.pushButton_8.clicked.connect(self.predict_video)

        self.export_onnx = False

    def update_text_browser(self, text):
        """将标准输出的结果显示到 QTextBrowser"""
        self.textBrowser.append(text)

    def on_training_finished(self):
        """训练结束时更新界面"""
        self.textBrowser.append("训练完成，模型已保存至指定目录。")

    def switch_to_train_page(self):
        self.stackedWidget.setCurrentIndex(0)

    def switch_to_predict_page(self):
        self.stackedWidget.setCurrentIndex(1)

    def select_yaml(self):
        """选择YAML配置文件"""
        self.yaml_path, _ = QFileDialog.getOpenFileName(self, "选择 YAML 文件", "", "YAML Files (*.yaml)")
        if self.yaml_path:
            self.textBrowser_2.setText(self.yaml_path)

    def toggle_onnx_export(self, state):
        self.export_onnx = state == Qt.Checked

    def start_training_process(self):
        """启动训练"""
        epochs = int(self.textEdit_3.toPlainText())
        imgsz = int(self.textEdit_4.toPlainText())
        batch_size = int(self.textEdit_5.toPlainText())
        learning_rate = float(self.textEdit_6.toPlainText())
        device = self.textEdit_7.toPlainText()
        workers = int(self.textEdit_8.toPlainText())
        momentum = float(self.textEdit_9.toPlainText())
        weight_decay = float(self.textEdit_10.toPlainText())
        yaml_path = self.yaml_path
        selected_model = self.comboBox.currentText()

        if yaml_path:
            self.textBrowser.append(f"正在使用 {selected_model} 模型进行训练...")

            self.training_thread = TrainingThread(
                self.model_paths[selected_model],
                yaml_path,
                epochs,
                imgsz,
                batch_size,
                learning_rate,
                device,
                workers,
                momentum,
                weight_decay,
                self.export_onnx
            )

            self.training_thread.output_signal.connect(self.update_text_browser)
            self.training_thread.finished_signal.connect(self.on_training_finished)
            self.training_thread.start()

    def select_trained_model(self):
        """选择训练好的模型"""
        self.trained_model_path, _ = QFileDialog.getOpenFileName(self, "选择训练好的模型", "", "Model Files (*.pt)")
        if self.trained_model_path:
            self.textBrowser_5.setText(self.trained_model_path)

    def predict_image(self):
        """预测图片"""
        if self.trained_model_path:
            image_path, _ = QFileDialog.getOpenFileName(self, "选择图片", "", "Image Files (*.png *.jpg *.bmp)")
            if image_path:
                self.textBrowser_6.append(f"开始预测图片：{image_path}")
                start_time = time.time()

                model = YOLO(self.trained_model_path)
                results = model(image_path)

                result_image = results[0].plot()
                q_image = self.convert_cv_qt(result_image)
                self.label_7.setPixmap(q_image)

                end_time = time.time()
                self.textBrowser_6.append(f"推理时间：{(end_time - start_time):.4f} 秒")

    def predict_video(self):
        """预测视频"""
        if self.trained_model_path:
            video_path, _ = QFileDialog.getOpenFileName(self, "选择视频", "", "Video Files (*.mp4 *.avi *.mov)")
            if video_path:
                self.textBrowser_6.append(f"开始预测视频：{video_path}")
                self.cap = cv2.VideoCapture(video_path)
                model = YOLO(self.trained_model_path)

                while self.cap.isOpened():
                    ret, frame = self.cap.read()
                    if not ret:
                        break

                    start_time = time.time()

                    results = model.predict(source=frame)

                    annotated_frame = results[0].plot()
                    q_image = self.convert_cv_qt(annotated_frame)
                    self.label_7.setPixmap(q_image)

                    end_time = time.time()
                    self.textBrowser_6.append(f"推理时间：{(end_time - start_time):.4f} 秒")

                self.cap.release()

    def convert_cv_qt(self, cv_img):
        rgb_image = cv2.cvtColor(cv_img, cv2.COLOR_BGR2RGB)
        h, w, ch = rgb_image.shape
        bytes_per_line = ch * w
        convert_to_Qt_format = QImage(rgb_image.data, w, h, bytes_per_line, QImage.Format_RGB888)

        q_pixmap = QPixmap.fromImage(convert_to_Qt_format)
        scaled_pixmap = q_pixmap.scaled(self.label_7.width(), self.label_7.height(), Qt.KeepAspectRatio, Qt.SmoothTransformation)

        self.label_7.setAlignment(Qt.AlignCenter)
        return scaled_pixmap

if __name__ == '__main__':
    multiprocessing.freeze_support()
    if sys.platform.startswith('win'):
        mp.set_start_method('spawn', force=True)

    app = QApplication(sys.argv)
    main_window = MainWindow()
    main_window.show()
    sys.exit(app.exec_())
